import React from "react";
import ReactDOM from "react-dom";
import CommentDetail from "./CommentDetail";
import ApprovalCard from "./ApprovalCard";
import faker from "faker";

const App = () => {
    return (
        <div className="ui container comments">
            <ApprovalCard>
                <CommentDetail
                    author="Sam"
                    date={faker.date.past().toDateString()}
                    avatar={faker.image.avatar()}
                    content={faker.lorem.words(15)}
                />
            </ApprovalCard>
            <ApprovalCard>
                <CommentDetail
                    author="Alex"
                    date={faker.date.past().toDateString()}
                    avatar={faker.image.avatar()}
                    content={faker.lorem.words(4)}
                />
            </ApprovalCard>
            <ApprovalCard>
                <CommentDetail
                    author="Jane"
                    date={faker.date.past().toDateString()}
                    avatar={faker.image.avatar()}
                    content={faker.lorem.words(7)}
                />
            </ApprovalCard>
        </div>
    );
}

ReactDOM.render(<App />, document.querySelector("#root"));
